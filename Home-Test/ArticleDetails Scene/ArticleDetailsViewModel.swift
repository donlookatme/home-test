//
//  ArticleDetailsViewModel.swift
//  Home-Test
//
//  Created by wansik jang on 2019-08-01.
//  Copyright © 2019 wansik jang. All rights reserved.
//

import Foundation

class ArticleDetailsViewModel {
    let pageModel: ArticleModel
    
    init(pageModel: ArticleModel) {
        self.pageModel = pageModel
    }
}
