//
//  ArticleDetailsViewController.swift
//  Home-Test
//
//  Created by wansik jang on 2019-08-01.
//  Copyright © 2019 wansik jang. All rights reserved.
//

import UIKit

/// This viewController displays details of article selected from the main screen.
/// If the contents(image + body text) is bigger than its content view, scroll will be enabled, otherwise disabled.

class ArticleDetailsViewController: UIViewController {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var bodyTextView: UITextView!
    
    var viewModel: ArticleDetailsViewModel? {
        didSet {
            loadPage()
        }
    }

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    private func loadPage() {
        self.title =  viewModel?.pageModel.articleDetails?.title
        self.bodyTextView.text = viewModel?.pageModel.articleDetails?.bodyText ?? ""

        imageView.downloadImage(url: viewModel?.pageModel.articleDetails?.thumbNail) {
            self.view.layoutIfNeeded()
        }
    }
}
