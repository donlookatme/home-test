//
//  Service.swift
//  Home-Test
//
//  Created by wansik jang on 2019-07-31.
//  Copyright © 2019 wansik jang. All rights reserved.
//

import Foundation
import RxSwift

class Service {
    let sourceUrl = "https://www.reddit.com/r/swift/.json"
    
    typealias ArticleDataClosure = (ArticleDataModel) -> Void
    typealias ErrorClosure = (Error?) -> Void
    
    
    /// to fetch 'Swift News' JSON feed from Reddit
    ///
    /// - Parameters:
    ///   - success: ArticleDataModel
    ///   - failure: Error
    func fetchArticleData(success: @escaping ArticleDataClosure, failure: @escaping ErrorClosure) {
        if let url = URL(string: sourceUrl) {
            let networkProcessor = NetworkProcessor(url: url)
            networkProcessor.fetchDataFromUrl(successHandler: { (data) in
                success(data)
            }) { (error) in
                failure(error)
            }
        }
    }
}
