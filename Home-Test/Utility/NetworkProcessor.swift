//
//  NetworkProcessor.swift
//  Home-Test
//
//  Created by wansik jang on 2019-07-31.
//  Copyright © 2019 wansik jang. All rights reserved.
//

import Foundation

class NetworkProcessor {
    
    lazy var configuration: URLSessionConfiguration = URLSessionConfiguration.default
    lazy var session: URLSession = URLSession(configuration: self.configuration)
    
    private let url: URL
    
    init(url: URL) {
        self.url = url
    }
    
    func fetchDataFromUrl(successHandler: @escaping (ArticleDataModel) -> Void, errorHandler: @escaping (Error?) -> Void ) {
        
        let request = URLRequest(url: url)
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            
            guard error == nil else {
                errorHandler(error)
                return
            }
            
            if let httpResponse = response as? HTTPURLResponse {
                switch httpResponse.statusCode {
                case 200:
                    //success; handle only 200 for test purpose
                    if let data = data {
                        let decoder = JSONDecoder()
                        if let articleData = try? decoder.decode(ArticleDataModel.self, from: data) {
                            successHandler(articleData)
                            print(articleData)
                        }
                    }
                default:
                    print("HTTP Reponse Code: \(httpResponse.statusCode)")
                }
            }
        }
        dataTask.resume()
    }
}
