//
//  UIImageViewExtension.swift
//  Home-Test
//
//  Created by wansik jang on 2019-08-01.
//  Copyright © 2019 wansik jang. All rights reserved.
//

import UIKit

let imageCache = NSCache<AnyObject, AnyObject>()

extension UIImageView {
    func downloadImage(url: URL?, completion: @escaping () -> Void) {
        self.image = nil
        guard let imageUrl = url, verifyUrl(url: imageUrl) else {
            return
        }
        
        if let imageFromCache = imageCache.object(forKey: imageUrl as AnyObject) as? UIImage {
            self.image = imageFromCache
            completion()
            return
        }
        
        URLSession.shared.dataTask(with: imageUrl) {
            data, response, error in
            if let data = data {
                DispatchQueue.main.async {
                    if let imageToCache = UIImage(data: data) {
                        imageCache.setObject(imageToCache, forKey: imageUrl as AnyObject)
                        self.image = imageToCache
                        completion()
                    }
                }
            }
            }.resume()
    }
    
    private func verifyUrl(url: URL) -> Bool {
        return UIApplication.shared.canOpenURL(url)
    }
}
