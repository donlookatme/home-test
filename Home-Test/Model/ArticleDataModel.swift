//
//  ArticleModel.swift
//  Home-Test
//
//  Created by wansik jang on 2019-07-31.
//  Copyright © 2019 wansik jang. All rights reserved.
//

import Foundation

public struct ArticleDataModel: Decodable {
    var articles: [ArticleModel]?
    
    private enum RootCodingKeys: String, CodingKey {
        case data
        
        enum NestedKey: String, CodingKey {
            case children
            
            enum SecondNestedKey: String, CodingKey {
                case data
            }
        }
    }
    
    public init(from decoder: Decoder) throws {
        let rootContainer = try decoder.container(keyedBy: RootCodingKeys.self)
        let nestedContainer = try rootContainer.nestedContainer(keyedBy: RootCodingKeys.NestedKey.self, forKey: .data)
        self.articles = try nestedContainer.decode([ArticleModel].self, forKey: .children)
    }
}

public struct ArticleModel: Decodable {
    var articleDetails: ArticleDetailsModel?
    
    private enum JsonKeys: String, CodingKey {
        case articleDetails = "data"
    }
    
    init() {
        self.articleDetails = nil
    }
    
    public init(from decoder: Decoder) throws {
        let dataContainer = try decoder.container(keyedBy: JsonKeys.self)
        self.articleDetails = try dataContainer.decode(ArticleDetailsModel.self, forKey: .articleDetails)
    }
}


public struct ArticleDetailsModel: Decodable {
    var title: String?
    var thumbNail: URL?
    var bodyText: String?
    
    private enum JsonKeys: String, CodingKey {
        case title
        case thumbNail = "thumbnail"
        case bodyText = "selftext"
    }
    
    public init(from decoder: Decoder) throws {
        let dataContainer = try decoder.container(keyedBy: JsonKeys.self)
        self.title = try dataContainer.decode(String.self, forKey: .title)
        self.thumbNail = try dataContainer.decode(URL.self, forKey: .thumbNail)
        self.bodyText = try dataContainer.decode(String.self, forKey: .bodyText)
    }
}


