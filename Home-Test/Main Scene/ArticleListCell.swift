//
//  ArticleListCell.swift
//  Home-Test
//
//  Created by wansik jang on 2019-07-31.
//  Copyright © 2019 wansik jang. All rights reserved.
//

import UIKit
import RxSwift

public enum CellAction {
    /// emit this event to update cell size after image is downloaded and set
    case imageDidSet(cell: UITableViewCell)
    /// emit this event to navigate to details page with corresponding model
    case cellSelected(model: ArticleModel?)
}

class ArticleListCell: UITableViewCell {

    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var thumbnailImageView: UIImageView! 
    @IBOutlet weak var cellButton: UIButton!
    
    var model: ArticleModel?
    
    private let articleCellSubject = PublishSubject<CellAction>()
    var articleCellObservable: Observable<CellAction> {
        return articleCellSubject.asObservable()
    }
    var disposebag = DisposeBag()

    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposebag = DisposeBag()
    }
    
    func configureCell(with model: ArticleModel) {
        self.model = model
        titleLabel.text = model.articleDetails?.title ?? ""
        thumbnailImageView.downloadImage(url: model.articleDetails?.thumbNail) {[unowned self] in
            self.stackView.layoutIfNeeded()
            self.articleCellSubject.onNext(.imageDidSet(cell: self))
        }
    }

    @IBAction func cellTapped(_ sender: Any) {
        self.articleCellSubject.onNext(.cellSelected(model: self.model))
    }
}
