//
//  MainViewModel.swift
//  Home-Test
//
//  Created by wansik jang on 2019-07-31.
//  Copyright © 2019 wansik jang. All rights reserved.
//

import Foundation
import RxSwift
import RxRelay

class MainViewModel {
    let service: Service
    let articleListData: BehaviorRelay<[ArticleModel]>
    
    init() {
        self.service = Service()
        articleListData = BehaviorRelay(value: [])
    }
    
    func fetchData() {
        service.fetchArticleData(success: {[weak self] (articleData) in
            guard let self = self else { return }
            if let articles = articleData.articles {
                self.articleListData.accept(articles)
            }
        }) {[weak self] (error) in
            if let error = error {
                self?.handleError(error)
            }
        }
    }

    func handleError(_ error: Error) {
        /// handle error
    }
    
}
