//
//  MainViewController.swift
//  Home-Test
//
//  Created by wansik jang on 2019-07-31.
//  Copyright © 2019 wansik jang. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa

struct kMainView {
    struct CellIdentifiers {
        static let articleListCell = "ArticleListCell"
    }
    static let storyboardName = "Main"
    static let articleDetailsVC = "ArticleDetailsViewController"
    static let pageTitle = "Swift News"
}

class MainViewController: UIViewController {

    @IBOutlet weak var mainTableView: UITableView!
    
    let viewModel = MainViewModel()
    let disposebag = DisposeBag()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bind()
        viewModel.fetchData()
        
        self.title = kMainView.pageTitle
    }
    
    private func bind() {
        /// binding data source in the viewModel to tableview
        viewModel.articleListData.asObservable()
            .bind(to: mainTableView.rx.items) {[unowned self] (tableview, row, model) in
                if let cell = tableview.dequeueReusableCell(withIdentifier: kMainView.CellIdentifiers.articleListCell, for: IndexPath(row: row, section: 0)) as? ArticleListCell {
                    cell.configureCell(with: model)
                    /// binding cell actions
                    cell.articleCellObservable.subscribe(onNext: { (cellAction) in
                        switch cellAction {
                        case .imageDidSet(let cell):
                            if self.isVisible(cell) {
                                self.refreshTableView()
                            }
                        case .cellSelected(model: let articleModel):
                            if let article = articleModel {
                                self.navigateToArticleScene(model: article)
                            }
                        }
                    }).disposed(by: cell.disposebag)
                    return cell
                }
                return UITableViewCell()
            }.disposed(by: disposebag)
    }
    
    private func refreshTableView() {
        mainTableView.beginUpdates()
        mainTableView.endUpdates()
    }
    
    private func isVisible(_ cell: UITableViewCell) -> Bool {
        return !mainTableView.visibleCells.filter({ (visibleCell) -> Bool in
            return cell == visibleCell
        }).isEmpty
    }
    
    private func navigateToArticleScene(model: ArticleModel) {
        let storyboard = UIStoryboard(name: kMainView.storyboardName, bundle: nil)
        if let vc = storyboard.instantiateViewController(withIdentifier: kMainView.articleDetailsVC) as? ArticleDetailsViewController{
            
            _ = vc.view
            let viewModel = ArticleDetailsViewModel(pageModel: model)
            vc.viewModel = viewModel
            
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }
}

